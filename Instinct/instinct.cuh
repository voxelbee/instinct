#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include "MemoryMapped.h"
#include <thread>
#include <future>
#include "queue.h"

#define EMPTY 0
#define FILLING 1
#define FILLED 2
#define UPLOADING 3
#define READY 4
#define RUNNING 5
#define DOWNLOADING_1 6
#define DOWNLOADING_2 7
#define DOWNLOADED 8
#define EMPTYING 9
#define connection_count 100

struct MetaData
{
	int neurons_per_thread;
	int buffer_count;
	int neuron_count;
	bool isCompressed;
	int voltage_threshold;
	int connection_distance;
	int voltage_decay_time;
	int output_decay_time;
	int reinforce_mult;
	int resting_voltage;
	int size_x;
	int size_y;
	int size_z;
};

#include "neuron.h"

class Instinct
{
private:
	enum LogType
	{
		Info,
		Warning,
		Critical
	};

	bool is_allocated;

	// The time in the network so that we know how long it has been since a neuron was called.
	unsigned long time;

	// The cuda streams that each buffer is ran in to upload, download and to run the kernel.
	cudaStream_t *buffer_streams;

	// The buffer states to show what stage that buffer is on if it is downlaoding, uploading, waiting, finished and running.
	char *buffer_states;

	// All of the neurons in the network.
	Neuron *neurons;

	// The meta data for the current network.
	MetaData *meta;

	// The device veriable for the meta data of current network.
	MetaData *dev_meta;

	MemoryMapped *mapped_file;

	std::string log_list;

	std::string str_project_name;

	cudaDeviceProp* device_properties;

	int *h_data_count;
	int *dev_data_count;

	Neuron *h_neurons;
	Neuron *dev_neurons;

	float *h_current_voltages;
	float *dev_current_voltages;

	float *h_next_voltages;
	float *dev_next_voltages;

	int *h_next_locations;
	int *dev_next_locations;

	std::thread *buffer_threads_empty;
	std::thread *buffer_threads_fill;

	std::mutex *buffer_fill_mutex;
	std::condition_variable *buffer_fill_conditions;

	bool *buffer_fill_process;

	std::mutex *buffer_empty_mutex;
	std::condition_variable *buffer_empty_conditions;

	bool *buffer_empty_process;

	bool shutdown_main = false;
	bool shutdown = false;

	std::thread thread;

	int number_threads;
	int block_count;

	Queue<int> locations_queue;
	Queue<float> voltages_queue;

	int *inverse_index;

	void createNetworkFiles(const std::string &project_name, MetaData *settings);
	std::string input(const std::string &string);
	bool toBool(const std::string &string);
	MetaData* setupSettings();
	Neuron* loadNeurons(const std::string &project_name);
	MetaData* loadMeta(const std::string &project_name);
	void log(const std::string &string, LogType type);
	void writeLog(const std::string &string, const std::string &project_name);
	int getSizeOfGPUData(MetaData *settings, cudaDeviceProp *in_device_properties);
	void logDeviceProperties(cudaDeviceProp* device_prop, int device_count);
	void allocateVars(MetaData *settings);
	void workerFillBuffer(int buffer);
	void workerEmptyBuffer(int buffer);
	void createThreads();
	void stateLoop();
	bool isEmpty();
	void freeCuda();
	void writeFiles();
	void freeThreads();
	void setupNeurons(MetaData *settings, Neuron *neurons);

public:

	// Init function called to setup the network veriables.
	void init();

	// Called when the program is going to be closing.
	void shutDown();

	// The run function called to start the network.
	void run();
};