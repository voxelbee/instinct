#pragma once

#include "instinct.cuh"

/*
The well known binary "all" or "none"  1/0 spike response behavior.
In most neurons, propagation of an electrical signal occurs via "all" or "none" action potentials.
These neurons integrate their input stimulus and fire when the input stimulus exceeds a certain threshold.
*/
#define ACTION_NEURON 1

/*
The analog/graded response behavior.
Some neurons, such as the vertebrate photoreceptor sensory neuronstransmit information
in an analog or graded manner. This form of analog response is typically found in neurons
with processes that extend only short distances.
*/
#define GRADED_NEURON 2

/*
Multistable behavior.
Some neurons show multiple persistent firing states that reflect the history
of their synaptic inputs. For example, neurons respond to consecutive stimuli
with graded changes in firing frequency that remains stable after stimulus
presentation. The sustained levels of firing frequency can be increased or
decreased in an input specific manner.
*/
#define INTRINSIC_NEURON 3

/*
The bistable switch  behavior.
These neurons can be in two states, an "on' state characterized by persistent firing
and  and "off" state where no firing occurs.
The transition between these two states can be triggered by a brief stimulus.
*/
#define PLATEAU_NEURON 4

#define UNDEFINED_NEURON 0

struct Neuron
{
	/*
	An array that contains the global connection location of each
	connection to nearby neurons.
	*/
	unsigned int output_locations[connection_count];

	/*
	How quickly the voltage of a neuron will decay as it goes along
	the axon.
	*/
	char output_decay;

	/*
	The current voltage inside of the neuron. This value will go up
	when one of its connections fires and it will go down over time.
	*/
	float internal_voltage;

	/*
	The type of neuron that the neuron is. This will change
	the way that it works and calculates.
	*/
	char neuron_type;

	/*
	An offset to tell the neuron when to fire. This value
	gets added to VOLTAGE_THRESHOLD to work out when it will fire.
	*/
	float threshold_offset;
};