#include <Windows.h>
#include "instinct.cuh"

// A exit handler function.
BOOL WINAPI consoleHandlerRoutine(DWORD dwCtrlType);

// True if the user has triggered the exit of the program.
bool isExit = false;

Instinct *instinct = new Instinct();

int main()
{
	// Setup the exit handler.
	BOOL ret = SetConsoleCtrlHandler(consoleHandlerRoutine, TRUE);

	// Set the maxium ram for the process.
	HANDLE process = ::GetCurrentProcess();
	size_t min = (2 ^ 30) * 1;
	size_t max = (2 ^ 30) * 10;
	::SetProcessWorkingSetSize(process, min, max);

	// Call the init function.
	instinct->init();

	// Run the network.
	instinct->run();

	return 0;
}

/*
Exit handler that is called when the program needs to exit
We have 5 seconds till the program is terminated.
*/
BOOL WINAPI consoleHandlerRoutine(DWORD dwCtrlType)
{
	instinct->shutDown();

	return FALSE;
}