#define NOMINMAX
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <iostream>
#include "instinct.cuh"
#include <Windows.h>
#include <string>
#include <sstream>
#include <direct.h>
#include <filesystem>
#include <fstream>
#include <cstdio>
#include "MemoryMapped.h"
#include <thread>
#include <future>
#include <algorithm>

__global__ void cudaMain(int *data_count, Neuron *neurons, float *current_voltages, int *next_locations, float *next_voltages, MetaData* meta, int num_neurons);

// Init function called to setup the network veriables.
void Instinct::init()
{ 
	is_allocated = false;
	log("Instinct 0.0.1 started on windows...", LogType::Info);

	device_properties = new cudaDeviceProp();
	cudaGetDeviceProperties(device_properties, 0);
	logDeviceProperties(device_properties, 1);

	std::string new_network = input("Do you want to create a new network? y/n: ");

	if (new_network == "y")
	{
		meta = setupSettings();
		str_project_name = input("What do you want to call the project?: ");
		createNetworkFiles(str_project_name, meta);

		neurons = loadNeurons(str_project_name);
		setupNeurons(meta, neurons);
	}
	else if (new_network == "n")
	{
		bool correct = false;
		while (!correct)
		{
			str_project_name = input("What is the name of the project to load?: ");

			if (GetFileAttributesA(("C:/Network/" + str_project_name).c_str()) == INVALID_FILE_ATTRIBUTES)
			{
				log("Something is wrong with your path", LogType::Warning);
			}
			else if (GetFileAttributesA(("C:/Network/" + str_project_name).c_str()) & FILE_ATTRIBUTE_DIRECTORY)
			{
				log("This is a project!", LogType::Info); 
				correct = true;
			}
			else
			{
				log("That directory does not exist", LogType::Warning);
			}
		}

		neurons = loadNeurons(str_project_name);
		meta = loadMeta(str_project_name);
	}

	allocateVars(meta);
	createThreads();
	is_allocated = true;
}

// Called when the program is going to be closing.
void Instinct::shutDown()
{
	log("Exiting...", LogType::Info);
	shutdown_main = true;

	freeThreads();
	writeFiles();
	freeCuda();
	writeLog(log_list, str_project_name);
}

// The run function called to start the network.
void Instinct::run()
{
	log("Running...", LogType::Info);
	thread = std::thread(&Instinct::stateLoop, this);
	while (!shutdown)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	std::this_thread::sleep_for(std::chrono::seconds(1));
}

// Create the network file. This has to be called if there is no network file.
void Instinct::createNetworkFiles(const std::string &project_name, MetaData *settings)
{
	mkdir(("C:/Network/" + project_name).c_str());

	HANDLE _file = ::CreateFileA(("C:/Network/" + project_name + "/" + "Neurons.bin").c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	LARGE_INTEGER size;
	size.QuadPart = sizeof(Neuron) * settings->neuron_count;
	::SetFilePointerEx(_file, size, NULL, FILE_BEGIN);
	::SetEndOfFile(_file);
	::CloseHandle(_file);

	FILE *pFile;
	pFile = fopen(("C:/Network/" + project_name + "/" + "Meta.bin").c_str(), "wb");

	fwrite(settings, sizeof(MetaData), 1, pFile);
	fclose(pFile);
}

std::string Instinct::input(const std::string &string)
{
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hstdout, 0x08);

	std::string input;
	printf((string + "\n").c_str());
	std::getline(std::cin, input);
	return input;
}

bool Instinct::toBool(const std::string &string)
{
	return string == "true";
}

MetaData* Instinct::setupSettings()
{
	MetaData *data = new MetaData();
	data->buffer_count = std::stoi(input("How many transfer buffers? default 4: "));
	data->connection_distance = std::stoi(input("How far do connections go in one direction? default 127: "));
	data->isCompressed = toBool(input("Do you want to use compression? default false: "));
	data->neurons_per_thread = std::stoi(input("How may neurons do you want per thread? default 10: "));
	data->output_decay_time = std::stoi(input("How Long should it take for the outputs to decay in network time? default 200: "));
	data->reinforce_mult = std::stoi(input("How much should the outputs reinforce? default 100: "));
	data->resting_voltage = std::stoi(input("What should the resting voltage be for a neuron? default -12: "));
	data->size_x = std::stoi(input("How big should the network be in neurons on one dimension? default 100: "));
	data->size_y = data->size_x;
	data->size_z = data->size_x;
	data->neuron_count = data->size_x * data->size_y * data->size_z;
	data->voltage_decay_time = std::stoi(input("How quickly will the voltage of a neiron decay in network time? default 50: "));
	data->voltage_threshold = std::stoi(input("What should the base voltage threashold be at? default 45: "));
	return data;
}

Neuron* Instinct::loadNeurons(const std::string &project_name)
{
	mapped_file = new MemoryMapped(("C:/Network/" + project_name + "/" + "Neurons.bin").c_str(), MemoryMapped::WholeFile, MemoryMapped::Normal);

	if (mapped_file == NULL)
	{
		log("Memory mapped file is null.", LogType::Critical);
		writeLog(log_list, project_name);
		system("exit");
	}

	Neuron* data = (Neuron*)mapped_file->getData();
	return data;
}

MetaData* Instinct::loadMeta(const std::string &project_name)
{
	FILE *pFile = fopen(("C:/Network/" + project_name + "/" + "Meta.bin").c_str(), "rb");
	if (!pFile)
	{
		log("Error: could not open file C:/Network/" + project_name + "/" + "Meta.bin", LogType::Critical);
	}

	MetaData *data = new MetaData();
	fread(data, sizeof(MetaData), 1, pFile);
	fclose(pFile);
	return data;
}

void Instinct::log(const std::string &string, LogType type)
{
	SYSTEMTIME *time = new SYSTEMTIME();
	GetSystemTime(time);
	std::string str_time = std::to_string(time->wHour) + ":" + std::to_string(time->wMinute) + ":" + std::to_string(time->wSecond);

	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	if (type == LogType::Info)
	{
		SetConsoleTextAttribute(hstdout, 0x08);
		printf(("[" + str_time +" INFO]: " + string + "\n").c_str());
		log_list += "[" + str_time +" INFO]: " + string + "\n";
	}
	else if (type == LogType::Warning)
	{
		SetConsoleTextAttribute(hstdout, 0x06);
		printf(("[" + str_time + " WARNING]: " + string + "\n").c_str());
		log_list += "[" + str_time + " WARNING]: " + string + "\n";
	}
	else if (type == LogType::Critical)
	{
		SetConsoleTextAttribute(hstdout, 0x04);
		printf(("[" + str_time + " CRITICAL]: " + string + "\n").c_str());
		log_list += "[" + str_time + " CRITICAL]: " + string + "\n";
	}
}

void Instinct::writeLog(const std::string &string, const std::string &project_name)
{
	mkdir(("C:/Network/" + project_name + "/Logs").c_str());

	SYSTEMTIME *time = new SYSTEMTIME();
	GetSystemTime(time);
	std::string str_time = std::to_string(time->wYear) + "-" + std::to_string(time->wMonth) + "-" + std::to_string(time->wDay) + "-" + std::to_string(time->wHour + 1);
	int file_number = 0;

	for (int i = 0; i < 100; i++)
	{
		if (!std::tr2::sys::exists("C:/Network/" + project_name + "/Logs/" + str_time + "-" + std::to_string(i) + ".log"))
		{
			file_number = i;
			break;
		}
		if (i == 99)
		{
			log("Maximum log files reached for this date: " + str_time, LogType::Warning);
		}
	}

	log("Writing log file for " + str_time, LogType::Info);

	std::ofstream out(("C:/Network/" + project_name + "/Logs/" + str_time + "-" + std::to_string(file_number) + ".log").c_str());
	out << string.c_str();
	out.close();
}

int Instinct::getSizeOfGPUData(MetaData *settings, cudaDeviceProp *in_device_properties)
{
	int num_threads = in_device_properties->multiProcessorCount * in_device_properties->maxThreadsPerMultiProcessor;
	int size_neurons = (sizeof(Neuron) * settings->neurons_per_thread) * num_threads;
	int size_volages = (sizeof(float) * settings->neurons_per_thread) * num_threads;
	int size_locations = (sizeof(int) * settings->neurons_per_thread) * num_threads;
	return size_neurons + (sizeof(float) * 2) + (sizeof(char) *104) + size_volages + size_locations;
}

void Instinct::logDeviceProperties(cudaDeviceProp* device_prop, int device_count)
{
	for (int i = 0; i < device_count; i++)
	{
		log("Major revision number:         " +  std::to_string(device_prop->major), LogType::Info);
		log("Minor revision number:         " + std::to_string(device_prop->minor), LogType::Info);
		log("Name:                          " + std::string(device_prop->name), LogType::Info);
		log("Total global memory:           " + std::to_string(device_prop->totalGlobalMem), LogType::Info);
		log("Total shared memory per block: " + std::to_string(device_prop->sharedMemPerBlock), LogType::Info);
		log("Total registers per block:     " + std::to_string(device_prop->regsPerBlock), LogType::Info);
		log("Warp size:                     " + std::to_string(device_prop->warpSize), LogType::Info);
		log("Maximum memory pitch:          " + std::to_string(device_prop->memPitch), LogType::Info);
		log("Maximum threads per block:     " + std::to_string(device_prop->maxThreadsPerBlock), LogType::Info);
		log("Warps per block:               " + std::to_string(device_prop->maxThreadsPerBlock / device_prop->warpSize), LogType::Info);
		for (int i = 0; i < 3; ++i)
		{
			if (i == 0)
			{
				log("Maximum dimension x of block:  " + std::to_string(device_prop->maxThreadsDim[i]), LogType::Info);
			}
			else if (i == 1)
			{
				log("Maximum dimension y of block:  " + std::to_string(device_prop->maxThreadsDim[i]), LogType::Info);
			}
			else if (i == 2)
			{
				log("Maximum dimension z of block:  " + std::to_string(device_prop->maxThreadsDim[i]), LogType::Info);
			}
		}
		for (int i = 0; i < 3; ++i)
		{
			if (i == 0)
			{
				log("Maximum x dimension of grid:   " + std::to_string(device_prop->maxGridSize[i]), LogType::Info);
			}
			else if (i == 1)
			{
				log("Maximum y dimension of grid:   " + std::to_string(device_prop->maxGridSize[i]), LogType::Info);
			}
			else if (i == 2)
			{
				log("Maximum z dimension of grid:   " + std::to_string(device_prop->maxGridSize[i]), LogType::Info);
			}
		}
		log("Clock rate:                    " + std::to_string(device_prop->clockRate), LogType::Info);
		log("Total constant memory:         " + std::to_string(device_prop->totalConstMem), LogType::Info);
		log("Texture alignment:             " + std::to_string(device_prop->textureAlignment), LogType::Info);
		log("Concurrent copy and execution: " + std::string((device_prop->deviceOverlap ? "Yes" : "No")), LogType::Info);
		log("Number of multiprocessors:     " + std::to_string(device_prop->multiProcessorCount), LogType::Info);
		log("Kernel execution timeout:      " + std::string((device_prop->kernelExecTimeoutEnabled ? "Yes" : "No")), LogType::Info);
		log("Amount of free memory:         " + std::to_string(device_prop->totalConstMem), LogType::Info);
		log("Total memory:                  " + std::to_string(device_prop->totalGlobalMem), LogType::Info);
		log("asyncEngineCount:              " + std::to_string(device_prop->asyncEngineCount), LogType::Info);
	}
}

void Instinct::allocateVars(MetaData *settings)
{
	buffer_states = new char[settings->buffer_count];
	buffer_streams = new cudaStream_t[settings->buffer_count];
	for (int i = 0; i < settings->buffer_count; i++)
	{
		buffer_states[i] = EMPTY;
		cudaStreamCreate(&buffer_streams[i]);
	}

	log("Created buffer state array to a size of " + std::to_string(sizeof(char) * settings->buffer_count), LogType::Info);
	log("Created buffer streams array to a size of " + std::to_string(sizeof(cudaStream_t) * settings->buffer_count), LogType::Info);

	int number_threads = device_properties->multiProcessorCount * device_properties->maxThreadsPerMultiProcessor;

	cudaHostAlloc(&h_neurons, sizeof(Neuron) * ((number_threads * settings->neurons_per_thread) * settings->buffer_count), cudaHostAllocDefault);
	cudaMalloc(&dev_neurons, sizeof(Neuron) * ((number_threads * settings->neurons_per_thread) * settings->buffer_count));
	long size_neurons = sizeof(Neuron) * ((number_threads * settings->neurons_per_thread) * settings->buffer_count);
	log("Allocated host and device veriables of neuorns to a size of " + std::to_string(size_neurons), LogType::Info);

	cudaHostAlloc(&h_current_voltages, sizeof(float) * ((number_threads * settings->neurons_per_thread) * settings->buffer_count), cudaHostAllocDefault);
	cudaMalloc(&dev_current_voltages, sizeof(float) * ((number_threads * settings->neurons_per_thread) * settings->buffer_count));
	long size_current_voltages = sizeof(float) * ((number_threads * settings->neurons_per_thread) * settings->buffer_count);
	log("Allocated host and device veriables of current voltages to a size of " + std::to_string(size_current_voltages), LogType::Info);

	cudaHostAlloc(&h_next_voltages, sizeof(float) * ((((number_threads * settings->neurons_per_thread) * connection_count) / 62) * settings->buffer_count), cudaHostAllocDefault);
	cudaMalloc(&dev_next_voltages, sizeof(float) * ((((number_threads * settings->neurons_per_thread) * connection_count) / 62) * settings->buffer_count));
	long size_next_voltages = sizeof(float) * ((((number_threads * settings->neurons_per_thread) * connection_count) / 62) * settings->buffer_count);
	log("Allocated host and device veriables of next voltages to a size of " + std::to_string(size_next_voltages), LogType::Info);

	cudaHostAlloc(&h_next_locations, sizeof(int) * ((((number_threads * settings->neurons_per_thread) * connection_count) / 62) * settings->buffer_count), cudaHostAllocDefault);
	cudaMalloc(&dev_next_locations, sizeof(int) * ((((number_threads * settings->neurons_per_thread) * connection_count) / 62) * settings->buffer_count));
	long size_next_locations = sizeof(int) * ((((number_threads * settings->neurons_per_thread) * connection_count) / 62) * settings->buffer_count);
	log("Allocated host and device veriables of next locations to a size of " + std::to_string(size_next_locations), LogType::Info);

	cudaHostAlloc(&h_data_count, sizeof(int) * settings->buffer_count, cudaHostAllocDefault);
	cudaMalloc(&dev_data_count, sizeof(int) * settings->buffer_count);
	long size_data_count = sizeof(int) * settings->buffer_count;
	log("Allocated host and device veriables of data count to a size of " + std::to_string(size_data_count), LogType::Info);

	cudaMalloc(&dev_meta, sizeof(MetaData));
	long size_meta = sizeof(MetaData);
	log("Allocated device veriables of meta data to a size of " + std::to_string(size_meta), LogType::Info);

	cudaMemcpy(meta, dev_meta, sizeof(MetaData), cudaMemcpyHostToDevice);

	inverse_index = new int[(number_threads * settings->neurons_per_thread) * settings->buffer_count];

	log("Total allocation of " + std::to_string(((size_neurons+size_current_voltages+size_next_voltages+size_next_locations+size_data_count+size_meta))/(double)1000000000) + " Gigabytes", LogType::Info);
}

__global__ void cudaMain(int *data_count, Neuron *neurons, float *current_voltages, int *next_locations, float *next_voltages, MetaData* meta, int num_threads)
{
	
}

void Instinct::workerFillBuffer(int buffer)
{
	while (true)
	{
		std::unique_lock<std::mutex> lk(buffer_fill_mutex[buffer]);
		buffer_fill_conditions[buffer].wait(lk, [&] {return buffer_fill_process[buffer];});
		if (shutdown)
		{
			break;
		}
		buffer_fill_process[buffer] = false;

		size_t buffer_size = std::min(locations_queue.size(), (size_t)number_threads * meta->neurons_per_thread);
		locations_queue.popFront(inverse_index + buffer, buffer_size);
		voltages_queue.popFront(h_current_voltages + buffer, buffer_size);

		size_t i = 0;
		for (; i < buffer_size; i++)
		{
			(h_neurons + buffer)[i] = (h_neurons + buffer)[(inverse_index + buffer)[i]];
		}
		for (; i < number_threads * meta->neurons_per_thread; i++)
		{
			(h_neurons + buffer)[i].neuron_type = UNDEFINED_NEURON;
		}
		buffer_states[buffer] = FILLED;
	}
}

void Instinct::workerEmptyBuffer(int buffer)
{
	while (true)
	{
		std::unique_lock<std::mutex> lk(buffer_empty_mutex[buffer]);
		buffer_empty_conditions[buffer].wait(lk, [&] {return buffer_empty_process[buffer]; });
		if (shutdown)
		{
			break;
		}
		buffer_empty_process[buffer] = false;

		locations_queue.pushBack(h_next_locations + buffer, static_cast<size_t>(*h_data_count + buffer));
		voltages_queue.pushBack(h_next_voltages + buffer, static_cast<size_t>(*h_data_count + buffer));

		for (size_t i = 0; i < number_threads * meta->neurons_per_thread; i++)
		{
			if ((h_neurons + buffer)[i].neuron_type == UNDEFINED_NEURON)
			{
				break;
			}
			neurons[(inverse_index + buffer)[i]] = (h_neurons + buffer)[i];
		}
		buffer_states[buffer] = EMPTY;
	}
}

void Instinct::createThreads()
{
	buffer_empty_mutex = new std::mutex[meta->buffer_count];
	buffer_fill_mutex = new std::mutex[meta->buffer_count];
	buffer_fill_conditions = new std::condition_variable[meta->buffer_count];
	buffer_empty_conditions = new std::condition_variable[meta->buffer_count];
	buffer_fill_process = new bool[meta->buffer_count];
	buffer_empty_process = new bool[meta->buffer_count];
	buffer_threads_empty = new std::thread[meta->buffer_count];
	buffer_threads_fill = new std::thread[meta->buffer_count];

	for (int i = 0; i < meta->buffer_count; i++)
	{
		buffer_fill_process[i] = false;
		buffer_empty_process[i] = false;
		buffer_threads_empty[i] = std::thread(&Instinct::workerEmptyBuffer, this, i);
		buffer_threads_fill[i] = std::thread(&Instinct::workerFillBuffer, this, i);
	}
}

void Instinct::stateLoop()
{
	number_threads = device_properties->multiProcessorCount * device_properties->maxThreadsPerMultiProcessor;
	bool buffer_running = false;
	block_count = number_threads / 1024;

	while (!shutdown)
	{
		for (int i = 0; i < meta->buffer_count; i++)
		{
			char state = buffer_states[i];

			if (state == FILLED)
			{
				buffer_states[i] = UPLOADING;
				//printf("UPLOADING buffer");
				log("UPLOADING buffer: " + std::to_string(i), LogType::Info);

				cudaMemcpyAsync(h_neurons + i, dev_neurons + i, sizeof(Neuron) * (number_threads * meta->neurons_per_thread), cudaMemcpyHostToDevice, buffer_streams[i]);
				cudaMemcpyAsync(h_current_voltages + i, dev_current_voltages + i, sizeof(float) * (number_threads * meta->neurons_per_thread), cudaMemcpyHostToDevice, buffer_streams[i]);
			}
			else if (state == UPLOADING && cudaStreamQuery(buffer_streams[i]) == cudaSuccess)
			{
				buffer_states[i] = READY;
				//printf("READY buffer");
				log("READY buffer: " + std::to_string(i), LogType::Info);
			}
			else if (state == READY && !buffer_running)
			{
				buffer_states[i] = RUNNING;
				//printf("RUNNING buffer");
				log("RUNNING buffer: " + std::to_string(i), LogType::Info);
				buffer_running = true;

				cudaMain<<<block_count, 1024, 0, buffer_streams[i]>>>(dev_data_count, dev_neurons, dev_current_voltages, dev_next_locations, dev_next_voltages, dev_meta, number_threads);
			}
			else if (state == RUNNING && cudaStreamQuery(buffer_streams[i]) == cudaSuccess)
			{
				cudaMemcpyAsync(h_neurons + i, dev_neurons + i, sizeof(Neuron) * (number_threads * meta->neurons_per_thread), cudaMemcpyDeviceToHost, buffer_streams[i]);
				cudaMemcpyAsync(h_data_count + i, dev_data_count + i, sizeof(int), cudaMemcpyDeviceToHost, buffer_streams[i]);

				buffer_running = false;
				buffer_states[i] = DOWNLOADING_1;
				//printf("DOWNLOADING_1 buffer");
				log("DOWNLOADING_1 buffer: " + std::to_string(i), LogType::Info);
			}
			else if (state == DOWNLOADING_1 && cudaStreamQuery(buffer_streams[i]) == cudaSuccess)
			{
				cudaMemcpyAsync(h_next_locations + i, dev_next_locations + i, sizeof(int) * h_data_count[i], cudaMemcpyDeviceToHost, buffer_streams[i]);
				cudaMemcpyAsync(h_next_voltages + i, dev_next_voltages + i, sizeof(float) * h_data_count[i], cudaMemcpyDeviceToHost, buffer_streams[i]);

				buffer_states[i] = DOWNLOADING_2;
				//printf("DOWNLOADING_2 buffer");
				log("DOWNLOADING_2 buffer: " + std::to_string(i), LogType::Info);
			}
			else if (state == DOWNLOADING_2 && cudaStreamQuery(buffer_streams[i]) == cudaSuccess)
			{
				buffer_states[i] = DOWNLOADED;
				log("DOWNLOADED buffer: " + std::to_string(i), LogType::Info);
				//printf("DOWNLOADED buffer");
			}
			else if (state == EMPTY && !shutdown_main)
			{
				buffer_states[i] = FILLING;
				log("FILLING buffer: " + std::to_string(i), LogType::Info);

				std::lock_guard<std::mutex> lk(buffer_fill_mutex[i]);
				buffer_fill_process[i] = true;
				buffer_fill_conditions[i].notify_one();

				//printf("FILLING buffer");
			}
			else if (state == DOWNLOADED)
			{
				buffer_states[i] = EMPTYING;
				//printf("EMPTYING buffer");
				log("EMPTYING buffer: " + std::to_string(i), LogType::Info);

				std::lock_guard<std::mutex> lk(buffer_empty_mutex[i]);
				buffer_empty_process[i] = true;
				buffer_empty_conditions[i].notify_one();
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

bool Instinct::isEmpty()
{
	for (int i = 0; i < meta->buffer_count; i++)
	{
		if (buffer_states[i] != EMPTY)
		{
			return false;
		}
	}
	return true;
}

void Instinct::freeCuda()
{
	if (is_allocated)
	{
		cudaFree(dev_data_count);
		cudaFree(dev_neurons);
		cudaFree(dev_current_voltages);
		cudaFree(dev_next_voltages);
		cudaFree(dev_next_locations);

		cudaFreeHost(h_data_count);
		cudaFreeHost(h_neurons);
		cudaFreeHost(h_current_voltages);
		cudaFreeHost(h_next_voltages);
		cudaFreeHost(h_next_locations);
	}
}

void Instinct::writeFiles()
{
	if (mapped_file != NULL)
	{
		mapped_file->close();
		mapped_file->~MemoryMapped();
	}

	FILE *pFile = fopen(("C:/Network/" + str_project_name + "/" + "Meta.bin").c_str(), "wb");
	if (!pFile)
	{
		log("Error: could not open file C:/Network/" + str_project_name + "/" + "Meta.bin", LogType::Critical);
	}

	fwrite(meta, sizeof(MetaData), 1, pFile);
	fclose(pFile);
}

void Instinct::freeThreads()
{
	while (!isEmpty())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	shutdown = true;

	for (int j = 0; j < meta->buffer_count; j++)
	{
		std::lock_guard<std::mutex> lk(buffer_fill_mutex[j]);
		buffer_fill_process[j] = true;
		buffer_fill_conditions[j].notify_one();

		std::lock_guard<std::mutex> fk(buffer_empty_mutex[j]);
		buffer_empty_process[j] = true;
		buffer_empty_conditions[j].notify_one();
	}
}

void Instinct::setupNeurons(MetaData *settings, Neuron *neurons)
{
	for (int i = 0; i < settings->neuron_count; i++)
	{
		neurons[i].internal_voltage = settings->resting_voltage;
		neurons[i].neuron_type = ACTION_NEURON;
		neurons[i].output_decay = 100;
		neurons[i].threshold_offset = 0;
		for (int j = 0; j < connection_count; j++)
		{
			neurons[i].output_locations[j] = (-settings->connection_distance + (rand() % (int)(settings->connection_distance - -settings->connection_distance + 1))) + i;
		}
	}
	log("Setup defult neuron values", LogType::Info);
}