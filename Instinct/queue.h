#pragma once
#include <deque>

template <typename type>
class Queue
{
public:
	Queue();
	~Queue();
	void popFront(type *dest, size_t count);
	void pushBack(type *src, size_t count);
	size_t size();

private:
	std::deque<type> queue;

	size_t back_pos;
	size_t front_pos;
};

template <typename type>
Queue<type>::Queue()
{
	back_pos = 0;
	front_pos = 0;
	queue.resize(1024);
}

template <typename type>
Queue<type>::~Queue()
{
	
}

template <typename type>
void Queue<type>::pushBack(type *src, size_t count)
{
	size_t back = back_pos;
	if ((back_pos + count) % queue.size() >= front_pos)
	{
		queue.resize(queue.size() + (count * 4));
	}
	back_pos = (back_pos + count) % queue.size();
	for (size_t i = 0; i < count; i++)
	{
		queue[(back + i) % queue.size()] = src[i];
	}
}

template <typename type>
void Queue<type>::popFront(type *dest, size_t count)
{
	if (size() < count)
	{
		throw std::runtime_error("Too many elements requested and not enough elements in the queue");
	}

	if (back_pos > front_pos)
	{
		size_t front = front_pos;
		front_pos += count;
		for (size_t i = 0; i < count; i++)
		{
			dest[i] = queue[i + front];
		}
	}
	else
	{
		size_t front = front_pos;
		front_pos = (front_pos + count) % queue.size();
		size_t i = 0;
		for (; front + i < queue.size() && i < count; i++)
		{
			dest[i] = queue[front + i];
		}
		size_t last = i;
		for (; i < count; i++)
		{
			dest[i] = queue[i - last];
		}
	}
}

template <typename type>
size_t Queue<type>::size()
{
	if (back_pos > front_pos)
	{
		return back_pos - front_pos;
	}
	else
	{
		return queue.size() - front_pos + back_pos;
	}
}